//
//  BeaconReminderViewController.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 01.07.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "BeaconReminderViewController.h"


@interface BeaconReminderViewController()


@end

@implementation BeaconReminderViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
   
        
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.messageTextField.text = appDelegate.objectNameToRemind;
}

- (IBAction)save:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setObjectNameToRemind:self.messageTextField.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
