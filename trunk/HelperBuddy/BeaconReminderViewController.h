//
//  BeaconReminderViewController.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 01.07.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeaconReminder.h"
#import "NotificationService.h"
#import "AppDelegate.h"

@interface BeaconReminderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)save:(id)sender;

@end
