//
//  AppDelegate.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SensorLocator.h"
#import "BeaconReminder.h"
#import "NotificationService.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate, BeaconReminderDelegate>

@property (strong, nonatomic) UIWindow* window;
@property (strong, nonatomic) NSString* objectNameToRemind;

@end
