//
//  BeaconReminder.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 01.07.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class BeaconReminder;
@protocol BeaconReminderDelegate
- (void) enterRegion;
- (void) exitRegion;
@end

@interface BeaconReminder : NSObject<CLLocationManagerDelegate>

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak,nonatomic) id <BeaconReminderDelegate> delegate;

- (void) searchingForUUID: (NSString*) uuidString withMajor: (int) major withMinor: (int) minor;


@end
