//
//  SearchSensorViewController.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "SearchSensorViewController.h"


@interface SearchSensorViewController()

@property NSArray* sensors;

@end

@implementation SearchSensorViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.sensorLocator = [SensorLocator sharedInstance];
        self.sensorLocator.delegate = self;
       
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    self.tableView.delegate = self;
    [self.sensorLocator startSearching];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.sensorLocator stopSearching];
}

#pragma  mark - SensorLocator delegate

- (void)updateState:(CBCentralManagerState)state
{
    
}

- (void)updateStatusLog:(NSString *)text{
    self.statusLabel.text = text;
}

- (void)foundSensors:(NSArray *)sensors{
    
    self.sensors = sensors;
    [self.tableView reloadData];
    for(CBPeripheral *peripheral in sensors){
        NSLog(@"SensorTag found: %@", peripheral.name);
    }
}

#pragma  mark - TableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.sensors count] == 0){
        //[self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ButtonBarStatus"]] ];
    }
    return [self.sensors count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SensorCell"];
        cell.tag = indexPath.row;
    
    CBPeripheral* peripheral = [self.sensors objectAtIndex:indexPath.row];
    
    cell.textLabel.text = peripheral.name;
    cell.detailTextLabel.text = [peripheral.identifier UUIDString];
    
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CBPeripheral *p = [self.sensors objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
        // Deselect it
        cell.accessoryType = UITableViewCellAccessoryNone;
        // Disconnect it
        [[SensorLocator sharedInstance] disconnectPeripheral:p];
        
    }
    else {
        // Select it
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [[SensorLocator sharedInstance] connectPeripheral:p];
        
    }
}


//- (void) deselectOtherCells:(UITableViewCell*) cell inTableView:(UITableView*) tableView{
//    
//    //Need to total each section
//    for (int i = 0; i < [tableView numberOfSections]; i++)
//    {
//        NSInteger rows =  [tableView numberOfRowsInSection:i];
//        for (int row = 0; row < rows; row++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:i];
//            UITableViewCell *currentCell = [tableView cellForRowAtIndexPath:indexPath];
//            if(cell != currentCell){
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
//        
//    }
//}


@end
