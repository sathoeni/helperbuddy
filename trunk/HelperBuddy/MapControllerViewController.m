//
//  LocationTaggerViewController.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 25.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "MapControllerViewController.h"

#define ACCEL_THRESHOLD ((double) 0.04)
#define MAX_ACCEL ((double) 0.25)
#define ZOOM_FACTOR ((double) 1.5)
#define MAX_TRANSITION_METER ((int) 100000)


@interface MapControllerViewController()

@property Boolean calibrated;
@property int zoomlevel;

@end

@implementation MapControllerViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.sensorMonitor = [SensorMonitor new];
        self.sensorMonitor.delegate = self;
        self.calibrated = FALSE;
        self.zoomlevel = 8;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    SensorLocator* sensorLocator = [SensorLocator sharedInstance];
    //start monitoring for currently selected device
    [self.sensorMonitor setSetupData:[SensorConfigurator makeSensorTagConfigForMapController]];
    [self.sensorMonitor startMonitoringForDevice:[sensorLocator getConnectedDevice]];
    
    CLLocationCoordinate2D startCoordinate = CLLocationCoordinate2DMake(47.379022, 8.541001);
    
    [self.mapView setCenterCoordinate:startCoordinate zoomLevel:self.zoomlevel animated:NO];
}

- (void)updateSensorButtons:(sensorTagValues *)values{
    
    if(values.buttonPressed == 2){
        [self zoom:--self.zoomlevel];
        self.calibrated = FALSE;
    }else if(values.buttonPressed == 1){
        [self zoom:++self.zoomlevel];
        self.calibrated = FALSE;
    }else if(values.buttonPressed == 3){
        if(self.mapView.mapType == MKMapTypeStandard){
            self.mapView.mapType =  MKMapTypeHybrid;
        }else{
            self.mapView.mapType = MKMapTypeStandard;
        }
    }
    else if(values.buttonPressed == 0){
        self.calibrated = TRUE;
    }
}

- (void)updateSensorData:(sensorTagValues *)values{
    
    double metersLong = 0;
    double metersLat = 0;
    
    if(values.accX > ACCEL_THRESHOLD || values.accX < -ACCEL_THRESHOLD){
        double angleX = 0;
        if (values.accX < 0){
            angleX = values.accX + ACCEL_THRESHOLD;
        }else{
            angleX = values.accX - ACCEL_THRESHOLD;
        }
        
        metersLong = [self calculateMetersLongFromXAngle:(angleX)];
    }
    
    if(values.accY > ACCEL_THRESHOLD || values.accY < -ACCEL_THRESHOLD){
        double angleY = 0;
        if (values.accY < 0){
            angleY = values.accY + ACCEL_THRESHOLD;
        }else{
            angleY = values.accY - ACCEL_THRESHOLD;
        }
        
        metersLat = -[self calculateMetersLongFromXAngle:(angleY)];
    }
    
//     NSLog(@"meters lat: %d, meters long: %d", metersLat, metersLong);
    
    if(metersLat != 0 || metersLong != 0){
        CLLocationCoordinate2D coordinate = [self translateCoord:self.mapView.centerCoordinate MetersLat:metersLat MetersLong:metersLong];
        CLLocationCoordinate2D normalizedCoordinate = [self normalizeCoordinates:coordinate];
        [self.mapView setCenterCoordinate:normalizedCoordinate animated:false];
        
    }
}


- (double) getCurrentViewDistance {
    
    CLLocationCoordinate2D topLeft, topRight;
    
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint neMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), mRect.origin.y);
    MKMapPoint nwMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMaxY(mRect));
    topLeft = MKCoordinateForMapPoint(neMapPoint);
    topRight = MKCoordinateForMapPoint(nwMapPoint);

    
    CLLocation *pointALocation = [[CLLocation alloc] initWithLatitude:topLeft.latitude longitude:topLeft.longitude];
    CLLocation *pointBLocation = [[CLLocation alloc] initWithLatitude:topRight.latitude longitude:topRight.longitude];
    double distanceMeters = [pointALocation getDistanceFrom:pointBLocation];
    return distanceMeters;
    //double distanceMiles = (distanceMeters / 1609.344);
}


- (double) calculateMetersLongFromXAngle:(double)angle{
    
    //    0.17   100000
    //    angle        x
    //
    //    x = 100000 * angle / 0.17
    
    //    double tmp = 100000.0 / angle;
    //    double result = tmp / 0.21;
    //
    //    return result;
    // TODO: use global const for 0.17
    double faktor = [self getCurrentViewDistance] / 952366.0;
    double meters = (MAX_TRANSITION_METER)  * angle / (MAX_ACCEL - ACCEL_THRESHOLD);
    return meters * faktor;
}


/**
 * This method calcualtes valid coordinates
 */
- (CLLocationCoordinate2D) normalizeCoordinates:(CLLocationCoordinate2D) coordinate {
    //NSLog(@"lat: %f, long: %f", coordinate.latitude, coordinate.longitude);
    
    // Handle longitude (Längengrad)
    CLLocationDegrees newLongitude;
    if(coordinate.longitude > 180){
        newLongitude = coordinate.longitude - 360;
    }else if(coordinate.longitude < -180){
        newLongitude = coordinate.longitude + 360;
    }else{
        newLongitude = coordinate.longitude;
    }
    
    // Handle latitude (Breitengrad)
    CLLocationCoordinate2D topLeft, bottomRight;
    //    topLeft = [self.mapView convertPoint:CGPointMake(0, 0) toCoordinateFromView:self.mapView];
    //    CGPoint pointBottomRight = CGPointMake(self.mapView.frame.size.width, self.mapView.frame.size.height);
    //    bottomRight = [self.mapView convertPoint:pointBottomRight toCoordinateFromView:self.mapView];
    
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint neMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), mRect.origin.y);
    MKMapPoint swMapPoint = MKMapPointMake(mRect.origin.x, MKMapRectGetMaxY(mRect));
    topLeft = MKCoordinateForMapPoint(neMapPoint);
    bottomRight = MKCoordinateForMapPoint(swMapPoint);
    
    
//    NSLog(@"topLeft:");
//    [self logCoordinates:topLeft];
//    NSLog(@"bottomright:");
//    [self logCoordinates:bottomRight];
    
    

    
    //    if(((topLeft.latitude < threshold) && (topLeft.latitude > 0.0)) || ((bottomRight.latitude > threshold) && (bottomRight.latitude < 0))){
    //        newLatitude = coordinate.latitude;
    //    }else{
    //        newLatitude = threshold;
    //    }
    //
    //    if((topLeft.latitude >= threshold) && (topLeft.latitude > 0.0)){
    //        newLatitude = self.mapView.centerCoordinate.latitude;
    //    }else{
    //        newLatitude = coordinate.latitude;
    //    }
    
    
    return CLLocationCoordinate2DMake(coordinate.latitude, newLongitude);
}



- (void) logCoordinates:(CLLocationCoordinate2D)coordinates {
//    NSLog(@"lat: %f, meters long: %f", coordinates.latitude, coordinates.longitude);
}

-(CLLocationCoordinate2D)translateCoord:(CLLocationCoordinate2D)coord MetersLat:(double)metersLat MetersLong:(double)metersLong{
    
    CLLocationCoordinate2D tempCoord;
    
    MKCoordinateRegion tempRegion = MKCoordinateRegionMakeWithDistance(coord, metersLat, metersLong);
    MKCoordinateSpan tempSpan = tempRegion.span;
    
    tempCoord.latitude = coord.latitude + tempSpan.latitudeDelta;
    tempCoord.longitude = coord.longitude + tempSpan.longitudeDelta;
    
    return tempCoord;
    
}

- (void) zoom:(int) zoomLevel{
    if(self.calibrated){
        [self.mapView setCenterCoordinate:self.mapView.centerCoordinate zoomLevel:zoomLevel animated:NO];
    }
}
@end
