//
//  SensorConfigurator.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 30.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorConfigurator : NSObject

//+ (id)sharedInstance;

+ (NSMutableDictionary *) makeSensorTagDefaultConfiguration;
+ (NSMutableDictionary *) makeSensorTagConfigForMapController;

@end
