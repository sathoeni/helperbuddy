//
//  SearchSensorViewController.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SensorLocator.h"

@interface SearchSensorViewController : UIViewController<SensorLocatorDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) SensorLocator* sensorLocator;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
