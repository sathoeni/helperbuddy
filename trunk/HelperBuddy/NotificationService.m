//
//  NotificationService.m
//  NotifyMe
//
//  Created by Sascha Thöni on 22.10.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "NotificationService.h"
#import "Guid.h"

@implementation NotificationService


+ (UILocalNotification*) createNotification:(NSDate*) date withMessage:(NSString*) message andData:(NSMutableDictionary*) data
{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return nil;
    localNotif.fireDate = date; // = [date dateByAddingTimeInterval:10];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
	// Notification details
    localNotif.alertBody = message;
	// Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    //localNotif.applicationIconBadgeNumber = 1;
    
	// Specify custom data for the notification
    //NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
    

    if(!data){
        data = [NSMutableDictionary new];
    }
    
    NSString* uniqueKey = [[Guid randomGuid] stringValueWithFormat: GuidFormatDashed];
    [data setObject:uniqueKey forKey:@"id"];
    NSLog(@"Create Notificiation with id: %@", uniqueKey);
    
    localNotif.userInfo = data;
    
    return localNotif;
}


+ (NSString*) getNotifcationIDFromNotification:(UILocalNotification*) notification
{
    return [notification.userInfo objectForKey:@"id"];
}

+ (void) dispatchNotification: (UILocalNotification*) notification
{
	// Schedule the notification only when date is not already past
    NSDate *currentDate = [NSDate date];
    if(notification && [ notification.fireDate compare:currentDate] == NSOrderedDescending)
    {
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

+(void) deletaNotification: (UILocalNotification*) notification
{
    if(notification)
    {
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
}

+ (void) deletaNotificationWithID:(NSString*) notificationID
{
    NSArray* allNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for(int i = 0; i < [allNotifications count]; i++)
    {
        UILocalNotification* localNotification = (UILocalNotification*) allNotifications[i];
        if([[NotificationService getNotifcationIDFromNotification:localNotification] isEqualToString:notificationID]){
             [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        }
    }
}


@end
