//
//  SensorLocator.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEDevice.h"

@class SensorLocator;
@protocol SensorLocatorDelegate
- (void) updateStatusLog: (NSString*) text;
- (void) updateState:(CBCentralManagerState) state;
- (void) foundSensors:(NSArray*) sensors;
@end



@interface SensorLocator : NSObject<CBCentralManagerDelegate,CBPeripheralDelegate>

@property (strong,nonatomic) CBCentralManager *m;
@property (strong,nonatomic) NSMutableArray *nDevices;
@property (strong,nonatomic) NSMutableArray *sensorTags;



@property (weak,nonatomic) id <SensorLocatorDelegate> delegate;

+ (id)sharedInstance;
- (CBPeripheral*) getConnectedDevice;
- (BOOL) isDeviceConnected;
- (void) connectPeripheral:(CBPeripheral*) peripheral;
- (void) disconnectPeripheral:(CBPeripheral*) peripheral;
- (void) disconnectAll;
- (void) startSearching;
- (void) stopSearching;

@end
