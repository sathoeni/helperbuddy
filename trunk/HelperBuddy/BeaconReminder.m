//
//  BeaconReminder.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 01.07.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "BeaconReminder.h"

@interface BeaconReminder()

@property CLProximity lastProximity;

@end

@implementation BeaconReminder

- (id)init{
    self = [super init];
    if(self){
        // Initialize location manager and set ourselves as the delegate
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    return self;
}


- (void) searchingForUUID: (NSString*) uuidString withMajor: (int) major withMinor: (int) minor{
    // Create a NSUUID with the same UUID as the broadcasting beacon
//    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"FA5F55D9-BC63-402E-A254-091B8FE8C991"];
     NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuidString];
    
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major minor:minor identifier:@"com.appcoda.testregion"];
    
    // Tell location manager to start monitoring for the beacon region
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
}


#pragma mark - CLLocationManager Deleagtes
- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    // We entered a region, now start looking for our target beacons!
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
    [self.delegate enterRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    [self.delegate exitRegion];
}

-(void)locationManager:(CLLocationManager*)manager didRangeBeacons:(NSArray*)beacons inRegion:(CLBeaconRegion*)region
{
    // Beacon found!
    CLBeacon *foundBeacon = [beacons firstObject];
    
    // Detect found beacon
    if(foundBeacon.proximity == CLProximityFar || foundBeacon.proximity == CLProximityImmediate || foundBeacon.proximity == CLProximityNear){
//        [self.delegate enterRegion];
    }
    
    // Detect transition from NEAR to FAR
//    if(self.lastProximity == CLProximityNear && foundBeacon.proximity == CLProximityFar){
//        [self.delegate exitRegion];
//    }
    
    switch (foundBeacon.proximity)
    {
        case CLProximityFar:
            NSLog(@"Far away");
            break;
        case CLProximityImmediate:
            NSLog(@"Immediate");
            break;
        case CLProximityNear:
            NSLog(@"Near");
            break;
        case CLProximityUnknown:
            NSLog(@"Unknown Distance");
            break;
//        default:
//            self.statusLabel.text = @"No Data";
    }
    
    // You can retrieve the beacon data from its properties
    //NSString *uuid = foundBeacon.proximityUUID.UUIDString;
    //NSString *major = [NSString stringWithFormat:@"%@", foundBeacon.major];
    //NSString *minor = [NSString stringWithFormat:@"%@", foundBeacon.minor];
    
    self.lastProximity = foundBeacon.proximity;
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    //The location manager calls this method whenever there is a boundary transition for a region.
    //It calls this method in addition to calling the locationManager:didEnterRegion: and locationManager:didExitRegion: methods.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
