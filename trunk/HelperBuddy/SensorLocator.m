//
//  SensorLocator.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "SensorLocator.h"

@interface SensorLocator()

@property (weak, nonatomic) CBPeripheral* connectedPeripheral;

@end

@implementation SensorLocator

+ (id)sharedInstance {
    static SensorLocator *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}


- (id) init {
    self = [super init];
    if (self) {
        self.m.delegate = self;
        self.nDevices = [[NSMutableArray alloc]init];
        self.sensorTags = [[NSMutableArray alloc]init];
    }
    return self;
}


#pragma mark - interface methods

- (void) startSearching
{
    if(!self.m){
        self.m = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
    }
}

- (void) stopSearching
{
    [self.m stopScan];
}

- (void)connectPeripheral:(CBPeripheral *)peripheral {
    if(peripheral.state != CBPeripheralStateConnected)
    {
        [self.m connectPeripheral:peripheral options:nil];
    }else{
        NSLog(@"Device already connected");
    }
    self.connectedPeripheral = peripheral;
}

- (void)disconnectPeripheral:(CBPeripheral *)peripheral{
    if(peripheral.state == CBPeripheralStateConnected)
    {
        [self.m cancelPeripheralConnection:peripheral];
        self.connectedPeripheral = nil;
        [self.delegate updateStatusLog:@"SensorTag disconnected"];
    }else{
        NSLog(@"Device already disonnected");
    }
}

- (void)disconnectAll{
    if([self isDeviceConnected]){
        [self.m cancelPeripheralConnection:self.connectedPeripheral];
        self.connectedPeripheral = nil;
        [self.delegate updateStatusLog:@"All devices disconnected"];
    }
}

- (CBPeripheral *)getConnectedDevice {
    if([self isDeviceConnected]){
        return self.connectedPeripheral;
    }else{
        return nil;
    }
}

- (BOOL)isDeviceConnected {
    return self.connectedPeripheral != nil && self.connectedPeripheral.state == CBPeripheralStateConnected;
}

#pragma mark - CBCentralManager delegate
/*
-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state != CBCentralManagerStatePoweredOn) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"BLE not supported !" message:[NSString stringWithFormat:@"CoreBluetooth return state: %d",central.state] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else {
        [central scanForPeripheralsWithServices:nil options:nil];
    }
}*/

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    //self.cBReady = false;
    [self.delegate updateState:central.state];
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CoreBluetooth BLE hardware is powered off");
            [self.delegate updateStatusLog:@"Bluetooth is off"];
            break;
        case CBCentralManagerStatePoweredOn:
            NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
            //self.cBReady = true;
            [self.m scanForPeripheralsWithServices:nil options:nil];
            [self.delegate updateStatusLog:@"Bluetoot is ready"];
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CoreBluetooth BLE hardware is resetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CoreBluetooth BLE state is unauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CoreBluetooth BLE state is unknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
            [self.delegate updateStatusLog:@"Bluetooth not supported"];
            break;
        default:
            break;
    }
}


-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    NSLog(@"Found a BLE Device : %@",peripheral);
    
    /* iOS 6.0 bug workaround : connect to device before displaying UUID !
     The reason for this is that the CFUUID .UUID property of CBPeripheral
     here is null the first time an unkown (never connected before in any app)
     peripheral is connected. So therefore we connect to all peripherals we find.
     */
    
    peripheral.delegate = self;
    [central connectPeripheral:peripheral options:nil];
    [self.nDevices addObject:peripheral];
    
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    [peripheral discoverServices:nil];
    
    if(peripheral == self.connectedPeripheral && peripheral.state == CBPeripheralStateConnected)
    {
        NSLog(@"SensorTag is connected");
        [self.delegate updateStatusLog:@"SensorTag connected"];
    }
}


#pragma  mark - CBPeripheral delegate

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    BOOL replace = NO;
    BOOL found = NO;
    NSLog(@"Services scanned !");
    // Do not disconnect if a device is choosen
    if(self.connectedPeripheral != peripheral){
        [self.m cancelPeripheralConnection:peripheral];
    }
    
    for (CBService *s in peripheral.services) {
        NSLog(@"Service found : %@",s.UUID);
        if ([s.UUID isEqual:[CBUUID UUIDWithString:@"F000AA00-0451-4000-B000-000000000000"]])  {
            NSLog(@"This is a SensorTag !");
            if(![self isDeviceConnected]){
                [self.delegate updateStatusLog:@"SensorTag Found"];
            }
            found = YES;
        }
    }
    if (found) {
        // Match if we have this device from before
        for (int ii=0; ii < self.sensorTags.count; ii++) {
            CBPeripheral *p = [self.sensorTags objectAtIndex:ii];
            if ([p isEqual:peripheral]) {
                [self.sensorTags replaceObjectAtIndex:ii withObject:peripheral];
                replace = YES;
            }
        }
        if (!replace) {
            [self.sensorTags addObject:peripheral];
        }
        
        //trigger the update
        NSArray *array = [NSArray arrayWithArray:self.sensorTags];
        [self.delegate foundSensors:array];
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didUpdateNotificationStateForCharacteristic %@ error = %@",characteristic,error);
}

-(void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWriteValueForCharacteristic %@ error = %@",characteristic,error);
}



@end
