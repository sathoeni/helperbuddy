//
//  NotificationService.h
//  NotifyMe
//
//  Created by Sascha Thöni on 22.10.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationService : NSObject

+ (UILocalNotification*) createNotification:(NSDate*) date withMessage:(NSString*) message andData:(NSDictionary*) data;
+ (void) dispatchNotification: (UILocalNotification*) notification;
+(void) deletaNotification: (UILocalNotification*) notification;
+ (void) deletaNotificationWithID:(NSString*) notificationID;
+ (NSString*) getNotifcationIDFromNotification:(UILocalNotification*) notification;

@end
