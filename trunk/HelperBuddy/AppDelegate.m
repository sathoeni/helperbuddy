//
//  AppDelegate.m
//  HelperBuddy
//
//  Created by Sascha Thöni on 20.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate()

@property BeaconReminder *beaconReminder;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.beaconReminder = [BeaconReminder new];
    self.beaconReminder.delegate = self;
    [self.beaconReminder searchingForUUID:@"FA5F55D9-BC63-402E-A254-091B8FE8C991" withMajor:1 withMinor:1];

    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SensorLocator sharedInstance] disconnectAll];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    // Handle the notificaton when the app is running
    NSLog(@"Recieved Notification %@",notif);
    
    [[UIApplication sharedApplication] cancelLocalNotification:notif];
    
    UIApplicationState state = [app applicationState];
    if (state == UIApplicationStateActive) {
        
        
        [self handleNotification:notif showAlert:YES];
        //[[self getEventViewDetailController] handleNotification:notif showAlert:YES];
        
    } else {
        //Do stuff that you would do if the application was not active
        //[[self getEventViewDetailController] handleNotification:notif showAlert:NO];
    }
}

- (void)handleNotification:(UILocalNotification *)notification showAlert:(BOOL)showAlert
{
    if(showAlert)
    {
        NSString *cancelTitle = @"Close";
        NSString *showTitle = @"Ok";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention!"
                                                            message:notification.alertBody
                                                           delegate:self
                                                  cancelButtonTitle:cancelTitle
                                                  otherButtonTitles:showTitle, nil];
        [alertView show];
    }
}




- (void)enterRegion{
    NSLog(@"Enter Region");
}

- (void)exitRegion{
    NSLog(@"Exit Region");
    
    // Create date in a few seconds
    NSDate* date = [[NSDate new] dateByAddingTimeInterval:5];
    
    // Create the message
    NSString* message = [self createNotificationString:@"Don't forget to take your %@ with you."];
    
    UILocalNotification* notification = [NotificationService createNotification:date withMessage:message andData:nil];
    [NotificationService dispatchNotification:notification];
}

- (NSString*) createNotificationString: (NSString*) formatString{
    if(self.objectNameToRemind){
        NSString* tmpString = [NSString stringWithFormat:formatString,self.objectNameToRemind];
        return tmpString;
    }else{
        return formatString;
    }
}


@end
