//
//  SensorMonitor.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 22.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEDevice.h"
#import "BLEUtility.h"
#import "Sensors.h"
#import "SensorLocator.h"
#import "SensorConfigurator.h"

@class SensorMonitor;
@protocol SensorMonitorDelegate
- (void) updateSensorData: (sensorTagValues*) values;
- (void) updateSensorButtons: (sensorTagValues*) values;
@end

@interface SensorMonitor : NSObject<CBPeripheralDelegate>


@property (strong,nonatomic) NSMutableArray* vals;
@property (strong,nonatomic) NSTimer* logTimer;
@property (strong,nonatomic) sensorTagValues* currentVal;
@property (strong, nonatomic) NSMutableDictionary *setupData;
@property (weak,nonatomic) id <SensorMonitorDelegate> delegate;


- (void) startMonitoringForDevice:(CBPeripheral*) device;
- (void) calibrateGyroscope;

// TODO: implement sensor calibrate methods

@end
