//
//  LocationTaggerViewController.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 25.06.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MKMapView+ZoomLevel.h"
#import "SensorLocator.h"
#import "SensorMonitor.h"
#import "SensorConfigurator.h"

@interface MapControllerViewController : UIViewController<SensorMonitorDelegate>

extern double const accelThreshold;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) SensorMonitor* sensorMonitor;

@end
