//
//  MKMapView+Zoomlevel.h
//  HelperBuddy
//
//  Created by Sascha Thöni on 03.07.14.
//  Copyright (c) 2014 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>

// MKMapView+ZoomLevel.h
#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

@end
